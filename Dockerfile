### BUILD image
FROM maven:3-jdk-11 as builder

# create app folder for sources
RUN mkdir -p /build
WORKDIR /build
COPY pom.xml /build
#Download all required dependencies into one layer
RUN mvn -B dependency:resolve dependency:resolve-plugins
#Copy source code
COPY src /build/src
# Build application
RUN mvn package

FROM tomcat:9-jre11-openjdk-slim-bullseye

COPY --from=builder /build/target/address-book-with-servlet-and-jsp-1.0.0-SNAPSHOT.war /usr/local/tomcat/webapps/ROOT.war