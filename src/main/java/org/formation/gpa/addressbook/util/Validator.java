package org.formation.gpa.addressbook.util;

public interface Validator<T> {

	Validation validate(T t);
	
}
